// SPDX-License-Identifier: Apache-2.0
package org.islandoftex.texdoc

import org.junit.jupiter.api.extension.ConditionEvaluationResult
import org.junit.jupiter.api.extension.ExecutionCondition
import org.junit.jupiter.api.extension.ExtensionContext

class EnabledIfTeXdocPresent : ExecutionCondition {
    override fun evaluateExecutionCondition(context: ExtensionContext): ConditionEvaluationResult {
        return if (TeXDoc.isAvailable)
            ConditionEvaluationResult.enabled("TeXdoc is present in the PATH")
        else
            ConditionEvaluationResult.disabled("TeXdoc is not present in the PATH")
    }
}
