// SPDX-License-Identifier: Apache-2.0
package org.islandoftex.texdoc

import java.io.IOException
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

internal class TeXDocVersionTest {
    @Test
    @DisplayName("Version string conforms to pattern")
    @ExtendWith(EnabledIfTeXdocPresent::class)
    @Throws(IOException::class)
    fun versionTest() {
        assertTrue(
            TeXDoc.version
                .matches("\\w+\\s\\d+\\.\\d+\\.*\\d*\\s\\(\\d+-\\d+-\\d+\\)".toRegex())
        )
    }
}
