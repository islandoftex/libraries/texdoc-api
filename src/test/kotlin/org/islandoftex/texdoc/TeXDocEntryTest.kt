// SPDX-License-Identifier: Apache-2.0
package org.islandoftex.texdoc

import java.io.IOException
import java.lang.reflect.InvocationTargetException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

internal class TeXDocEntryTest {
    @ParameterizedTest(name = "Handles entries for {0} correctly")
    @ValueSource(strings = ["geometry", "mathtools", "tikz"])
    @Throws(
        IOException::class,
        NoSuchMethodException::class,
        InvocationTargetException::class,
        IllegalAccessException::class
    )
    fun parseEntries(name: String) {
        val toParse = TeXDocEntryTest::class.java
            .getResource("/org/islandoftex/texdoc/$name.csv")
            .readText().split("\n")
        val toCompare = TeXDocEntryTest::class.java
            .getResource("/org/islandoftex/texdoc/" + name + "_output.csv")
            .readText().split("\n")
        // we are unit testing a private method, so let's reflect about it
        val parseMethod = TeXDoc::class.java
            .getDeclaredMethod("parse", List::class.java)
        parseMethod.isAccessible = true
        @Suppress("UNCHECKED_CAST")
        val entries = parseMethod.invoke(TeXDoc, toParse) as List<Entry>
        toCompare.zip(entries).forEach {
            assertEquals(it.first, it.second.toString())
        }
    }
}
