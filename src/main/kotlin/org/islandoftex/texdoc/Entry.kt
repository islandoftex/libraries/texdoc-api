// SPDX-License-Identifier: Apache-2.0
package org.islandoftex.texdoc

import java.nio.file.Path

/**
 * Implements a texdoc entry.
 *
 * @author Island of TeX
 * @version 1.0
 * @since 1.0
 *
 * @property score The score of the entry (how good or bad a match is).
 * @property path The path to the document which is the main part of the entry.
 * @property language The language associated with the entry (probably undefined).
 * @property description The description associated with the entry.
 */
data class Entry(
    val score: Double,
    val path: Path,
    val language: String,
    val description: String
) {
    companion object {
        /**
         * @property INDETERMINED_SCORE This is a placeholder value to describe an
         *   indetermined score as [score] is not nullable.
         */
        const val INDETERMINED_SCORE = Double.NaN

        /**
         * @property EMPTY_DESCRIPTION_PLACEHOLDER This is a placeholder to
         *   describe an empty description as [description] is not nullable.
         */
        const val EMPTY_DESCRIPTION_PLACEHOLDER = "No description"

        /**
         * @property EMPTY_LANGUAGE_PLACEHOLDER This is a placeholder to
         *   describe an empty language as [language] is not nullable.
         */
        const val EMPTY_LANGUAGE_PLACEHOLDER = "Unknown language"
    }

    /**
     * Provides a textual representation of this object.
     *
     * @return A textual representation of this object.
     */
    override fun toString(): String {
        return "($score, $path, $language, $description)"
    }
}
