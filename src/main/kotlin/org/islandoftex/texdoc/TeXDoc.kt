// SPDX-License-Identifier: Apache-2.0
package org.islandoftex.texdoc

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.nio.file.Paths
import java.util.regex.Matcher

/**
 * Implements the TeXDoc lookup.
 *
 * @author Island of TeX
 * @version 1.0
 * @since 1.0
 */
object TeXDoc {

    private const val SCORE_GROUP = 2
    private const val PATH_GROUP = 3
    private const val LANGUAGE_GROUP = 4
    private const val DESCRIPTION_GROUP = 5

    /**
     * Gets the output from a given command line application as a list of lines
     * (strings), to be parsed later.
     *
     * @param command The command to be executed.
     * @return A list of strings containing the lines of output of the command.
     * @throws IOException An I/O exception was thrown for some reason.
     */
    @Throws(IOException::class)
    private fun getCommandOutput(command: List<String>): List<String> {
        val process = ProcessBuilder(command).start()
        val input = BufferedReader(InputStreamReader(process.inputStream))
        return input.readLines()
    }

    /**
     * Gets the version number of the installed version of TeXDoc.
     *
     * @return The version string provided by texdoc. This includes the name,
     * the version and the release date.
     * @throws IOException If there is a problem fetching the version number
     * an I/O exception is thrown.
     */
    val version: String
        @Throws(IOException::class)
        get() = getCommandOutput(listOf("texdoc", "--version"))[0]

    /**
     * Checks whether texdoc is available in the system's PATH.
     */
    val isAvailable: Boolean by lazy {
        try {
            version.isNotBlank()
        } catch (_: Exception) {
            false
        }
    }

    /**
     * Gets the output from the texdoc command line application as a list of
     * strings, to be parsed later.
     *
     * @param keyword The keyword to be queried. Currently, we favour looking up
     * one keyword at a time instead of multiples queries at once.
     * @param includeBad Whether to include bad results in the output.
     * @return A list of strings containing the output of texdoc, given the
     * provided keyword.
     * @throws IOException An I/O exception was thrown for some reason.
     */
    @Throws(IOException::class)
    private fun getEntriesFor(keyword: String, includeBad: Boolean): List<String> =
        getCommandOutput(
            listOf(
                "texdoc", if (includeBad) "-s" else "-l",
                "-M", keyword
            )
        )

    /**
     * Parses the list of strings returned from the texdoc command line
     * application into a proper list of keywords.
     *
     * @param output The list strings from the output of the texdoc command line
     * application.
     * @return A list of entries, sorted according to texdoc (the index might
     * give a clue about the order as well).
     */
    private fun parse(output: List<String>): List<Entry> {
        if (output.isEmpty() || output[0].trim().startsWith("Sorry")) {
            return ArrayList()
        } else {
            // texdocs output:
            // search_keyword\t score\t path\t language (if given)\t description
            val pattern = """(\S*)\t(-?\d+\.*\d*)\t([^\t]+)\t([^\t]*)\t(.*)""".toPattern()
            return output.asSequence()
                .map { pattern.matcher(it) }
                .mapNotNull { matcher: Matcher ->
                    if (matcher.find()) {
                        Entry(
                            score = matcher.group(SCORE_GROUP).toDoubleOrNull() ?: Entry.INDETERMINED_SCORE,
                            path = Paths.get(matcher.group(PATH_GROUP)),
                            language = if (matcher.group(LANGUAGE_GROUP).isNullOrBlank())
                                Entry.EMPTY_LANGUAGE_PLACEHOLDER
                            else
                                matcher.group(LANGUAGE_GROUP),
                            description = if (matcher.group(DESCRIPTION_GROUP).isNullOrBlank())
                                Entry.EMPTY_DESCRIPTION_PLACEHOLDER
                            else
                                matcher.group(DESCRIPTION_GROUP)
                        )
                    } else null
                }.toList()
        }
    }

    /**
     * Returns a list of entries given the keyword from the texdoc command line
     * application.
     *
     * @param keyword The keyword to be queried by texdoc.
     * @param includeBad Whether to include bad results. By default they will be
     * excluded.
     * @return A list of entries, containing number, path and an optional
     * description from texdoc.
     * @throws IOException An I/O exception was thrown for some reason, probably
     * due to a missing command call.
     */
    @Throws(IOException::class)
    @JvmOverloads
    fun getEntries(keyword: String, includeBad: Boolean = false): List<Entry> {
        return parse(getEntriesFor(keyword, includeBad))
    }

    /**
     * Open the file associated with an entry through TeXdoc. This causes the
     * current thread to wait until texdoc has opened the file.
     *
     * @param entry The entry holding the path to the file which should be opened.
     * @return The exit value of texdoc.
     * @throws IOException If the texdoc process cannot be started.
     * @throws InterruptedException If we can't successfully wait for texdoc
     * to exit.
     */
    @Throws(IOException::class, InterruptedException::class)
    fun view(entry: Entry): Boolean {
        val builder = ProcessBuilder(
            "texdoc",
            "--just-view", entry.path.toAbsolutePath().toString()
        )
        val process = builder.start()
        return process.waitFor() == 0
    }
}
