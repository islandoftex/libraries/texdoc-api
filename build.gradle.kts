import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.gradle.api.tasks.testing.logging.TestLogEvent

plugins {
    kotlin("jvm") version "2.1.0"
    id("org.jetbrains.dokka") version "1.9.20"
    id("com.diffplug.spotless") version "6.25.0"
    id("com.diffplug.spotless-changelog") version "3.1.2"
    id("com.github.ben-manes.versions") version "0.51.0"
    id("io.gitlab.arturbosch.detekt") version "1.23.7"
    id("maven-publish")
    id("java-library")
}

if (!project.hasProperty("JobToken")) {
    logger.warn(
        """
            Island of TeX ----------------------------------------------
            Will be unable to publish (jobToken missing)
            Ignore this warning if you are not running the publish task
            for the GitLab package registry.
            ------------------------------------------------------------
        """.trimIndent()
    )
}

spotlessChangelog {
    changelogFile("CHANGELOG.md")
    setAppendDashSnapshotUnless_dashPrelease(true)
    ifFoundBumpBreaking("breaking change")
    tagPrefix("v")
    commitMessage("Release v{{version}}")
    remote("origin")
    branch("master")
}

group = "org.islandoftex"
status = "development"
description = "TeXdoc Java API"
version = project.spotlessChangelog.versionNext

repositories {
    mavenCentral()
}

java {
    JavaVersionProfile.AS_VERSION.let {
        sourceCompatibility = it
        targetCompatibility = it
    }

    withSourcesJar()
    withJavadocJar()
}

dependencies {
    testImplementation(kotlin("test"))
    implementation(kotlin("stdlib"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.11.3")
}

kotlin {
    jvmToolchain(JavaVersionProfile.AS_INTEGER)
}

tasks {
    named<Jar>("javadocJar") {
        from(dokkaJavadoc)
    }

    withType<Jar>().configureEach {
        manifest.attributes.apply {
            put("Implementation-Title", project.name)
            put("Implementation-Version", project.version)
            put("Automatic-Module-Name", "${group}.texdoc")
        }

        from(project.projectDir) {
            include("LICENSE.md")
            into("META-INF")
        }
    }

    withType<Test>().configureEach {
        useJUnitPlatform()

        testLogging {
            events(
                TestLogEvent.FAILED, TestLogEvent.PASSED, TestLogEvent.SKIPPED,
                TestLogEvent.STANDARD_ERROR, TestLogEvent.STANDARD_OUT
            )
        }
    }

    dokkaHtml {
        dokkaSourceSets {
            configureEach {
                jdkVersion.set(JavaVersionProfile.AS_INTEGER)
                moduleName.set("${group}.texdoc")
                includeNonPublic.set(false)
                skipDeprecated.set(false)
                reportUndocumented.set(true)
                skipEmptyPackages.set(true)
                platform.set(org.jetbrains.dokka.Platform.jvm)
                sourceLink {
                    localDirectory.set(file("./"))
                    remoteUrl.set(uri("https://gitlab.com/islandoftex/libraries/texdoc-api").toURL())
                    remoteLineSuffix.set("#L")
                }
                noStdlibLink.set(false)
                noJdkLink.set(false)
            }
        }
    }

    withType<DependencyUpdatesTask> {
        rejectVersionIf {
            val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { candidate.version.uppercase() in it }
            val isStable = stableKeyword || "^[0-9,.v-]+$".toRegex().matches(candidate.version)
            isStable.not()
        }
    }

}

spotless {
    kotlinGradle {
        trimTrailingWhitespace()
        endWithNewline()
    }

    kotlin {
        licenseHeader("// SPDX-License-Identifier: Apache-2.0")
        target("src/**/*.kt")
        trimTrailingWhitespace()
        endWithNewline()
    }
}

detekt {
    buildUponDefaultConfig = true
    config.setFrom(files("detekt-config.yml"))
}

publishing {
    publications {
        create<MavenPublication>("GitLab") {
            pom {
                name.set("Island of TeX TeXDoc Java API")
                description.set(
                    "This project provides a Java/Kotlin API for the texdoc command line interface. Hence, it is " +
                            "only one layer of abstraction but requires the presence of texdoc on the target machine."
                )
                inceptionYear.set("2019")
                url.set("https://gitlab.com/islandoftex/libraries/texdoc-api")
                organization {
                    name.set("Island of TeX")
                    url.set("https://gitlab.com/islandoftex")
                }
                licenses {
                    license {
                        name.set("Apache License Version 2.0")
                        url.set("https://gitlab.com/islandoftex/libraries/texdoc-api/blob/master/LICENSE")
                    }
                }
                scm {
                    connection.set("scm:git:git://gitlab.com/islandoftex/libraries/texdoc-api.git")
                    developerConnection.set("scm:git:ssh://git@gitlab.com:islandoftex/libraries/texdoc-api.git")
                    url.set("https://gitlab.com/islandoftex/libraries/texdoc-api")
                }
                ciManagement {
                    system.set("GitLab")
                    url.set("https://gitlab.com/islandoftex/libraries/texdoc-api/pipelines")
                }
                issueManagement {
                    system.set("GitLab")
                    url.set("https://gitlab.com/islandoftex/libraries/texdoc-api/issues")
                }
            }

            from(components["java"])
            artifacts {
                archives(tasks["sourcesJar"])
                archives(tasks["javadocJar"])
            }
        }
    }

    repositories {
        maven {
            name = "GitLab"
            url = uri("https://gitlab.com/api/v4/projects/14399565/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                if (project.hasProperty("jobToken")) {
                    name = "Job-Token"
                    value = project.property("jobToken").toString()
                }
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

object JavaVersionProfile {
    const val AS_INTEGER = 8
    val AS_VERSION = JavaVersion.VERSION_1_8
}
