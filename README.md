# TeXdoc API for JVM

![Language: Kotlin](https://img.shields.io/badge/Language-Kotlin-blue.svg?style=flat-square)
![Minimum JRE: 8.0](https://img.shields.io/badge/Minimum_JRE-8-blue.svg?style=flat-square)


## About the project

This project provides a Java/Kotlin API for the `texdoc` command line
interface. Hence, it is only one layer of abstraction but requires the
presence of `texdoc` on the target machine.

## Usage

The API provides two classes:

* the `Entry` class which represents an entry in TeXdoc's list of results.
* the `TeXDoc` class which is a singleton providing an interface to the
  commands and actions of the command line utility.

As the classes have been written in Kotlin you can access them natively in
Java. Please note, that TeXDoc will be translated as singleton with its
instance held in `TeXDoc.INSTANCE`.

The most important methods and fields of TeXDoc are:

* `isAvailable` (Java: `isAvailable()`) to check whether TeXdoc is in the PATH
* `version` (Java: `getVersion()`) to get the version string (`String`) of the
  TeXdoc installation
* `getEntries(keyword: String, includeBad: Boolean = false)` (Java: same
  signature, optional argument is overload) to get a `List<Entry>` containing
  all entries from the result set of a TeXdoc query with the keyword `keyword`.
  The optional parameter indicates whether bad matches (very low score) should
  be included.
* `view(entry: Entry)` to view the resource specified by an entry. It returns
  a boolean whether the process exited sucessfully.

## License

This library is licensed under the
[Apache License, version 2.0](https://opensource.org/licenses/Apache-2.0).
Please note that the Apache License, version 2.0, has been verified as a
GPL-compatible free software license by the
[Free Software Foundation](http://www.fsf.org/).


## The team

The TeXdoc API for JVM is brought to you by the Island of TeX. If you want to
support TeX development by a donation, the best way to do this is donating to
the [TeX Users Group](https://www.tug.org/donate.html).

