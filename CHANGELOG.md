# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.2.1] - 2024-12-14

### Changed

* Updated dependencies.
* Updated Kotlin to 2.1.0.
* Updated Gradle to 8.10.

## [1.2.0] - 2024-07-16

### Added

* Added support for the language field. (see #2)

## [1.1.1] - 2024-07-15

### Changed

* Updated dependencies.

## [1.1.0] - 2021-04-01

### Fixed

* Entries with negative scores are now supported. (see #1)

## [1.0.0] - 2020-04-16

* Initial release.

[Unreleased]: https://gitlab.com/islandoftex/libraries/texdoc-api/compare/v1.2.1...master
[1.2.1]: https://gitlab.com/islandoftex/libraries/texdoc-api/compare/v1.2.0...v1.2.1
[1.2.0]: https://gitlab.com/islandoftex/libraries/texdoc-api/compare/v1.1.1...v1.2.0
[1.1.1]: https://gitlab.com/islandoftex/libraries/texdoc-api/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/islandoftex/libraries/texdoc-api/compare/v1.0.0...v1.1.0
[1.0.0]: https://gitlab.com/islandoftex/libraries/texdoc-api/-/tags/v1.0.0
